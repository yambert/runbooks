<!-- MARKER: do not edit this section directly. Edit services/service-catalog.yml then run scripts/generate-docs -->

**Table of Contents**

[[_TOC_]]

#  Version Service
* **Alerts**: https://alerts.gitlab.net/#/alerts?filter=%7Btype%3D%22version%22%2C%20tier%3D%22sv%22%7D
* **Label**: gitlab-com/gl-infra/production~"Service:Version"

## Logging

* [production.log](/var/log/version/)

## Troubleshooting Pointers

* [../cloudflare/managing-traffic.md](../cloudflare/managing-traffic.md)
* [../cloudflare/services-locations.md](../cloudflare/services-locations.md)
* [../config_management/chef-guidelines.md](../config_management/chef-guidelines.md)
* [../config_management/chef-vault.md](../config_management/chef-vault.md)
* [../config_management/chef-workflow.md](../config_management/chef-workflow.md)
* [../config_management/chefspec.md](../config_management/chefspec.md)
* [../elastic/elastic-cloud.md](../elastic/elastic-cloud.md)
* [../gitaly/gitaly-error-rate.md](../gitaly/gitaly-error-rate.md)
* [../gitaly/storage-sharding.md](../gitaly/storage-sharding.md)
* [../logging/logging_gcs_archive_bigquery.md](../logging/logging_gcs_archive_bigquery.md)
* [../monitoring/apdex-alerts-guide.md](../monitoring/apdex-alerts-guide.md)
* [../monitoring/filesystem_alerts_inodes.md](../monitoring/filesystem_alerts_inodes.md)
* [../monitoring/thanos-compact.md](../monitoring/thanos-compact.md)
* [../packaging/manage-package-signing-keys.md](../packaging/manage-package-signing-keys.md)
* [../patroni/geo-patroni-cluster.md](../patroni/geo-patroni-cluster.md)
* [../patroni/patroni-management.md](../patroni/patroni-management.md)
* [../patroni/postgres-checkup.md](../patroni/postgres-checkup.md)
* [../patroni/postgres_exporter.md](../patroni/postgres_exporter.md)
* [../patroni/postgresql-backups-wale-walg.md](../patroni/postgresql-backups-wale-walg.md)
* [../pgbouncer/patroni-consul-postgres-pgbouncer-interactions.md](../pgbouncer/patroni-consul-postgres-pgbouncer-interactions.md)
* [../postgres-dr-delayed/postgres-dr-replicas.md](../postgres-dr-delayed/postgres-dr-replicas.md)
* [../praefect/praefect-read-only.md](../praefect/praefect-read-only.md)
* [../uncategorized/about-gitlab-com.md](../uncategorized/about-gitlab-com.md)
* [../uncategorized/aptly.md](../uncategorized/aptly.md)
* [../uncategorized/cloudsql-data-export.md](../uncategorized/cloudsql-data-export.md)
* [../uncategorized/dev-environment.md](../uncategorized/dev-environment.md)
* [../uncategorized/k8s-cluster-upgrade.md](../uncategorized/k8s-cluster-upgrade.md)
* [../uncategorized/k8s-oncall-setup.md](../uncategorized/k8s-oncall-setup.md)
* [../uncategorized/k8s-operations.md](../uncategorized/k8s-operations.md)
* [../uncategorized/manage-chef.md](../uncategorized/manage-chef.md)
* [../uncategorized/manage-pacemaker.md](../uncategorized/manage-pacemaker.md)
* [../uncategorized/manage-workers.md](../uncategorized/manage-workers.md)
* [../uncategorized/mtail.md](../uncategorized/mtail.md)
* [../uncategorized/omnibus-package-updates.md](../uncategorized/omnibus-package-updates.md)
* [../uncategorized/project-export.md](../uncategorized/project-export.md)
* [../uncategorized/remove-kernels.md](../uncategorized/remove-kernels.md)
* [../uncategorized/tweeting-guidelines.md](../uncategorized/tweeting-guidelines.md)
* [../uncategorized/upgrade-camoproxy.md](../uncategorized/upgrade-camoproxy.md)
* [../uncategorized/uptycs_osquery.md](../uncategorized/uptycs_osquery.md)
* [../uncategorized/yubikey.md](../uncategorized/yubikey.md)
* [gitaly-version-mismatch.md](gitaly-version-mismatch.md)
* [version-gitlab-com.md](version-gitlab-com.md)
* [../web/static-repository-objects-caching.md](../web/static-repository-objects-caching.md)
<!-- END_MARKER -->


<!-- ## Summary -->

<!-- ## Architecture -->

<!-- ## Performance -->

<!-- ## Scalability -->

<!-- ## Availability -->

<!-- ## Durability -->

<!-- ## Security/Compliance -->

<!-- ## Monitoring/Alerting -->

<!-- ## Links to further Documentation -->
