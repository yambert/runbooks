# frozen_string_literal: true

require 'rspec'
require 'kubernetes_rules'
require 'webmock/rspec'
require 'tmpdir'
require 'stringio'
